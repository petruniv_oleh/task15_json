package com.oleh.controller;

import com.oleh.model.Paper;
import com.oleh.model.parsers.GsonParser;
import com.oleh.model.parsers.JacksonParser;

import java.io.File;
import java.util.Comparator;
import java.util.List;

public class Controller implements ControllerInt {

    GsonParser gsonParser;
    JacksonParser jacksonParser;

    public Controller(File json) {
        gsonParser = new GsonParser(json);
        jacksonParser = new JacksonParser(json);
    }

    @Override
    public List<Paper> getParsedWithGson() {
        return gsonParser.getPapers();
    }

    @Override
    public List<Paper> getParsedWithJackson() {
        return jacksonParser.getPapers();
    }

    @Override
    public void sortFilewithGson(Comparator comparator) {
        gsonParser.sortJsonValuesInFile(comparator);
    }

    @Override
    public void sortFileWithJackson(Comparator comparator) {
        jacksonParser.sortValuesInFile(comparator);
    }
}
