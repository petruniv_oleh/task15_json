package com.oleh.controller;


import com.oleh.model.Paper;

import java.util.Comparator;
import java.util.List;

public interface ControllerInt {

    List<Paper> getParsedWithGson();
    List<Paper> getParsedWithJackson();
    void sortFilewithGson(Comparator comparator);
    void sortFileWithJackson(Comparator comparator);
}
