package com.oleh.model;

public class Paper {
    private String title;
    private String type;
    private boolean mounthly;
    private Chars chars;

    public Paper() {
    }

    public Paper(String title, String type, boolean mounthly, Chars chars) {
        this.title = title;
        this.type = type;
        this.mounthly = mounthly;
        this.chars = chars;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isMounthly() {
        return mounthly;
    }

    public void setMouthly(boolean mouthly) {
        this.mounthly = mouthly;
    }

    public Chars getChars() {
        return chars;
    }

    public void setChars(Chars chars) {
        this.chars = chars;
    }

    @Override
    public String toString() {
        return "Paper{" +
                "title='" + title + '\'' +
                ", type='" + type + '\'' +
                ", mounthly=" + mounthly +
                ", chars=" + chars.toString() +
                '}';
    }
}
