package com.oleh.model.parsers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oleh.model.Paper;

import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class GsonParser {

    private Gson gson;
    private List<Paper> papers;
    private File json;

    public GsonParser(File json) {
        papers = new CopyOnWriteArrayList<Paper>();
        gson = new GsonBuilder().create();
        this.json = json;
        parse();
    }

    private void parse() {
        Paper[] papersArr = new Paper[0];
        try {
            papersArr = gson.fromJson(new FileReader(json), Paper[].class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        papers = Arrays.asList(papersArr);
    }

    public List<Paper> getPapers() {
        return papers;
    }

    public void sortJsonValuesInFile(Comparator comparator) {
        papers.sort(comparator);
        try (FileWriter fileWriter = new FileWriter(json)) {
            gson.toJson(papers, fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
