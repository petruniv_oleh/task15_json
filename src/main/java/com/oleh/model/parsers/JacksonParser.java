package com.oleh.model.parsers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oleh.model.Paper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;


public class JacksonParser {
    private ObjectMapper objectMapper;
    private List<Paper> papers;
    private File json;

    public JacksonParser(File json) {
        objectMapper = new ObjectMapper();
        this.json = json;
        parse();
    }

    private void parse() {
        Paper[] papersArr = new Paper[0];

        try {
            papersArr = objectMapper.readValue(json, Paper[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        papers = Arrays.asList(papersArr);
    }

    public List<Paper> getPapers() {
        return papers;
    }

    public void sortValuesInFile(Comparator comparator){
        papers.sort(comparator);
        try(FileWriter fileWriter = new FileWriter(json)){
            objectMapper.writeValue(fileWriter, papers);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
