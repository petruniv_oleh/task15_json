package com.oleh.model.parsers;


import com.oleh.model.Paper;

import java.util.Comparator;

public class PapersComparator implements Comparator<Paper> {

    @Override
    public int compare(Paper o1, Paper o2) {
        return o1.getChars().getAmountOfPages()-o2.getChars().getAmountOfPages();
    }
}
