package com.oleh.model.validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;

import java.io.File;
import java.io.IOException;

public class JsonSchemaValidator {

    public static boolean validate(File json, File schema) throws IOException, ProcessingException {
        JsonNode jsonJson = JsonLoader.fromFile(json);
        JsonNode jsonSchema = JsonLoader.fromFile(schema);

        JsonSchemaFactory jsonSchemaFactory = JsonSchemaFactory.byDefault();
        JsonValidator validator = jsonSchemaFactory.getValidator();

        ProcessingReport validate = validator.validate(jsonSchema, jsonJson);
        return validate.isSuccess();

    }
}
