package com.oleh.view;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.oleh.controller.Controller;
import com.oleh.model.Paper;
import com.oleh.model.parsers.PapersComparator;
import com.oleh.model.validator.JsonSchemaValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.*;

public class View implements ViewInt {

    Controller controller;
    private Map<String, String> menuMap;
    private Map<String, Printable> menuMapMethods;
    private static Logger logger = LogManager.getLogger(View.class.getName());
    Scanner scanner = new Scanner(System.in);
    private String jsonPath;
    private String schemaPath;

    public View() {
        readProperties();
        File json = new File(jsonPath);
        File schema = new File(schemaPath);
        try {
            if (JsonSchemaValidator.validate(json, schema)) {
                controller = new Controller(json);
                menuMap();
                showMenu();
            } else {
                System.out.println("Json is not valid");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ProcessingException e) {
            e.printStackTrace();
        }

    }

    private void readProperties() {
        try (InputStream inputStream = new FileInputStream("src/main/resources/prop/json-path.properties")) {
            Properties properties = new Properties();
            properties.load(inputStream);
            jsonPath = properties.getProperty("json");
            schemaPath = properties.getProperty("schema");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void menuMap() {
        menuMap = new LinkedHashMap<>();
        menuMapMethods = new LinkedHashMap<>();
        menuMap.put("1", "1. Gson result");
        menuMap.put("2", "2. Jackson result");
        menuMap.put("3", "3. Gson sort file");
        menuMap.put("4", "4. Jackson sort file");


        menuMapMethods.put("1", this::gsonRes);
        menuMapMethods.put("2", this::jacksonRes);
        menuMapMethods.put("3", this::gsonSort);
        menuMapMethods.put("4", this::jacksonSort);


    }

    private void mapMenuOut() {
        logger.info("Menu out");
        System.out.println("\nMenu:");
        for (String s : menuMap.values()
        ) {
            System.out.println(s);
        }
    }

    @Override
    public void showMenu() {
        String key;
        do {
            mapMenuOut();
            System.out.println("====================");
            System.out.println("Select option:");
            key = scanner.nextLine().toUpperCase();
            logger.info("The key for a menu is " + key);
            try {
                menuMapMethods.get(key).print();

            } catch (Exception e) {
                logger.error("Some problem with the key");
            }
        } while (!key.equals("Q"));
    }

    @Override
    public void gsonRes() {
        logger.info("Invoke gsonRes");
        List<Paper> parsedWithGson =
                controller.getParsedWithGson();
        printList(parsedWithGson);

    }

    @Override
    public void jacksonRes() {
        logger.info("Invoke jacksonRes");
        List<Paper> parsedWithJackson = controller.getParsedWithJackson();
        printList(parsedWithJackson);
    }

    @Override
    public void gsonSort() {
        logger.info("Invoke gsonSort");
        controller.sortFilewithGson(new PapersComparator());
    }

    @Override
    public void jacksonSort() {
        logger.info("Invoke jacksonSort");
        controller.sortFileWithJackson(new PapersComparator());
    }


    private void printList(List list) {
        logger.info("printing list");
        for (Object o : list
        ) {
            System.out.println(o);
        }
    }
}
