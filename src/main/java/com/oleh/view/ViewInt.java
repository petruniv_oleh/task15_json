package com.oleh.view;

public interface ViewInt {
    void showMenu();
    void gsonRes();
    void jacksonRes();
    void gsonSort();
    void jacksonSort();
}
